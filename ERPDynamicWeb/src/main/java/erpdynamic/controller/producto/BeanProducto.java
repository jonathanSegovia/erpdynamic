package erpdynamic.controller.producto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import erpdynamic.model.entities.ProCategoria;
import erpdynamic.model.entities.ProDetalle;
import erpdynamic.model.productos.managers.ManagerProductos;
import erpdynamic.util.JSF.JSFUtil;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

@Named
@SessionScoped
public class BeanProducto implements Serializable {
	static final long serialVersionUID = 1L;
	@EJB
	private ManagerProductos mProducto;
	private List<ProDetalle> listaProductos;
	private List<ProCategoria> listaCategoria;
	private ProDetalle productoSeleccionado;
	private ProCategoria categoriaSeleccionada;
	private ProDetalle proDatelle;
	private ProCategoria proCategoria; 
	private int proCodigoCategoria;
	private String codDetalle;
	private String calidad;
	private String descripcion;
	private BigDecimal valor ;
	private double valor1;
	private int proCodigoDetalle;
	boolean panelColapsado;
	public BeanProducto() {

	}

	@PostConstruct
	public void inicializar() {
		listaProductos= mProducto.findAllProDetalles();
		listaCategoria= mProducto.findAllProCategorias();
		proDatelle= new ProDetalle();
		proCategoria= new ProCategoria();
		panelColapsado=true;
	}
	public void actionListenerColapsarPanel() {
		panelColapsado=!panelColapsado;
	}
	// detalle productos

	public void actionListenerInsertarProductos() {
		try {
			mProducto.insertarprodcutoes(productoSeleccionado, proCodigoDetalle, categoriaSeleccionada, calidad, descripcion, valor1);
			//mProducto.insertarProductos(proCodigoCategoria, proCodigoCategoria, calidad, descripcion, valor, proCategoria);
			//mProducto.insertarProd(proDatelle);
			//mProducto.insertarProductos(proCodigoCategoria, codDetalle, calidad, descripcion, valor, proCategoria);
			//mProducto.insertarProductos(proDatelle, codDetalle, calidad, descripcion, valor, proCategoria);
			//mProducto.insertarProducto(proDatelle);
			listaProductos= mProducto.findAllProDetalles();
			proDatelle= new ProDetalle();
			JSFUtil.crearMensajeInfo("Datos de Producto insertados.");
		} catch (Exception e) {
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void actionListenerEliminarProducto(String proCodigoDetalle) {
		mProducto.eliminarProducto(Integer.parseInt(proCodigoDetalle));
		listaProductos= mProducto.findAllProDetalles();
		JSFUtil.crearMensajeInfo("Producto eliminado");
	}
	
	public void actionListenerSeleccionarProducto(ProDetalle proCodigoDetalle) {
		productoSeleccionado= proCodigoDetalle;
	}
	
	public void actionListenerActualizarProducto() {
		try {
			mProducto.actualizarProducto(proDatelle, proCodigoCategoria);;
			listaProductos= mProducto.findAllProDetalles();
			JSFUtil.crearMensajeInfo("Datos actualizados.");
		} catch (Exception e) {
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}
	// Categoria

	public void actionListenerInsertarCategoria() {
		try {
			mProducto.insertarCategoria();;
			listaCategoria= mProducto.findAllProCategorias();
			proCategoria= new ProCategoria();
			JSFUtil.crearMensajeInfo("Datos de Categoria insertados.");
		} catch (Exception e) {
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void actionListenerEliminarCategoria(String proCodigoCategoria) {
		mProducto.eliminarProducto(Integer.parseInt(proCodigoCategoria));
		listaProductos= mProducto.findAllProDetalles();
		JSFUtil.crearMensajeInfo("Producto eliminado");
	}
	
	public void actionListenerSeleccionarCategoria(ProCategoria proCodigoCategoria) {
		categoriaSeleccionada= proCategoria ;
	}
	
	public void actionListenerActualizarCategoria() {
		try {
			mProducto.actualizarCategoria(proCategoria);
			mProducto.actualizarProducto(proDatelle, proCodigoCategoria);
			listaCategoria= mProducto.findAllProCategorias();
			
			JSFUtil.crearMensajeInfo("Datos actualizados.");
		} catch (Exception e) {
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}
	public String actionReportes(){
		 Map<String,Object> parametros=new HashMap<String,Object>();
		 /*parametros.put("p_titulo_principal",p_titulo_principal);
		 parametros.put("p_titulo",p_titulo);*/
		 FacesContext context=FacesContext.getCurrentInstance();
		 ServletContext servletContext=(ServletContext)context.getExternalContext().getContext();
		 String ruta=servletContext.getRealPath("inventario/reporteproducto.jasper");
		 System.out.println(ruta);
		 HttpServletResponse response=(HttpServletResponse)context.getExternalContext().getResponse();
		 response.addHeader("Content-disposition", "attachment;filename=reporteproducto.pdf");
		 response.setContentType("application/pdf");
		 try {
		 Class.forName("org.postgresql.Driver");
		 Connection connection = null;
		 connection = DriverManager.getConnection(
		 "jdbc:postgresql://192.100.198.141:5432/spgordonm","spgordonm", "1003676861");
		 JasperPrint impresion=JasperFillManager.fillReport(ruta, parametros,connection);
		 JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
		 context.getApplication().getStateManager().saveView(context);
		 System.out.println("reporte generado.");
		 context.responseComplete();
		 } catch (Exception e) {
			 JSFUtil.crearMensajeError(e.getMessage());
		 e.printStackTrace();
		 }
		 return "";
		 }

	public String actionReporte(){
		 Map<String,Object> parametros=new HashMap<String,Object>();
		 /*parametros.put("p_titulo_principal",p_titulo_principal);
		 parametros.put("p_titulo",p_titulo);*/
		 FacesContext context=FacesContext.getCurrentInstance();
		 ServletContext servletContext=(ServletContext)context.getExternalContext().getContext();
		 String ruta=servletContext.getRealPath("inventario/reportedetalle.jasper");
		 System.out.println(ruta);
		 HttpServletResponse response=(HttpServletResponse)context.getExternalContext().getResponse();
		 response.addHeader("Content-disposition", "attachment;filename=reportedetalle.pdf");
		 response.setContentType("application/pdf");
		 try {
		 Class.forName("org.postgresql.Driver");
		 Connection connection = null;
		 connection = DriverManager.getConnection(
		 "jdbc:postgresql://192.100.198.141:5432/spgordonm","spgordonm", "1003676861");
		 JasperPrint impresion=JasperFillManager.fillReport(ruta, parametros,connection);
		 JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
		 context.getApplication().getStateManager().saveView(context);
		 System.out.println("reporte generado.");
		 context.responseComplete();
		 } catch (Exception e) {
			 JSFUtil.crearMensajeError(e.getMessage());
		 e.printStackTrace();
		 }
		 return "";
		 }




	public List<ProDetalle> getListaProductos() {
		return listaProductos;
	}

	public void setListaProductos(List<ProDetalle> listaProductos) {
		this.listaProductos = listaProductos;
	}

	public List<ProCategoria> getListaCategoria() {
		return listaCategoria;
	}

	public void setListaCategoria(List<ProCategoria> listaCategoria) {
		this.listaCategoria = listaCategoria;
	}

	public ProDetalle getProductoSeleccionado() {
		return productoSeleccionado;
	}

	public void setProductoSeleccionado(ProDetalle productoSeleccionado) {
		this.productoSeleccionado = productoSeleccionado;
	}

	public ProCategoria getCategoriaSeleccionada() {
		return categoriaSeleccionada;
	}

	public void setCategoriaSeleccionada(ProCategoria categoriaSeleccionada) {
		this.categoriaSeleccionada = categoriaSeleccionada;
	}

	public ProDetalle getProDatelle() {
		return proDatelle;
	}

	public void setProDatelle(ProDetalle proDatelle) {
		this.proDatelle = proDatelle;
	}

	public ProCategoria getProCategoria() {
		return proCategoria;
	}

	public void setProCategoria(ProCategoria proCategoria) {
		this.proCategoria = proCategoria;
	}

	public int getProCodigoCategoria() {
		return proCodigoCategoria;
	}

	public void setProCodigoCategoria(int proCodigoCategoria) {
		this.proCodigoCategoria = proCodigoCategoria;
	}

	public String getCodDetalle() {
		return codDetalle;
	}

	public void setCodDetalle(String codDetalle) {
		this.codDetalle = codDetalle;
	}

	public String getCalidad() {
		return calidad;
	}

	public void setCalidad(String calidad) {
		this.calidad = calidad;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public boolean isPanelColapsado() {
		return panelColapsado;
	}

	public void setPanelColapsado(boolean panelColapsado) {
		this.panelColapsado = panelColapsado;
	}
	

}
