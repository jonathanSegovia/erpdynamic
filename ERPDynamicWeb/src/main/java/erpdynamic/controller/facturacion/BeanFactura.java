package erpdynamic.controller.facturacion;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import erpdynamic.model.entities.SegUsuario;

@Named
@RequestScoped
public class BeanFactura implements Serializable {
	private static final long serialVersionUID = 1L;
	private SegUsuario segUsuario;
	public BeanFactura() {
		// TODO Auto-generated constructor stub
	}

	public void openNewCliente() {
		this.segUsuario = new SegUsuario();
	}

	public SegUsuario getSegUsuario() {
		return segUsuario;
	}

	public void setSegUsuario(SegUsuario segUsuario) {
		this.segUsuario = segUsuario;
	}
	
	

}
