package erpdynamic.controller.facturacion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import erpdynamic.model.dtos.DTODetalleFactura;
import erpdynamic.model.entities.Cliente;
import erpdynamic.model.entities.ProDetalle;
import erpdynamic.model.facturacion.dtos.DTOFactura;
import erpdynamic.model.facturacion.managers.ManagerFacturacion;
import erpdynamic.util.JSF.JSFUtil;

@Named
@SessionScoped
public class BeanFacturacion implements Serializable {
	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerFacturacion managerFacturacion;
	
	private List<ProDetalle> listaproCategorias;
	private List<DTOFactura> listaFactura;
	private List<Cliente> listadoClientes;
	private String proCodigoCategoriaSeleccionado;
	private String proCalidad;
	private Cliente cliente;
	private boolean panelColapsado;
	private double total;
	private double iva;
	private double totalApagar;
	private String cedulaSeleccionado;

	public BeanFacturacion() {
		
	}
	
	@PostConstruct
	public void inicializar() {
		listaproCategorias=managerFacturacion.finAllProDetalle();
		listadoClientes=managerFacturacion.finAllCliente();
		//listaFactura=managerFacturacion.
		cliente= new Cliente();
		panelColapsado=true;
	}
	
	public void actionListenerColapsarPanel() {
		panelColapsado=!panelColapsado;
	}
	
	public void actionListenerInsertarCliente() {
			try {
				managerFacturacion.insertarCliente(cliente);
				//lista=managerClientes.findAllClientes();
				cliente=new Cliente();
				JSFUtil.crearMensajeInfo("Datos de estudiante insertados.");
			} catch (Exception e) {
				JSFUtil.crearMensajeError(e.getMessage());
				e.printStackTrace();
			}
		
	}
	
	public String actionListenerGuardarFactura() {
		try {
			managerFacturacion.insertarFactura(listaFactura, cedulaSeleccionado);
			JSFUtil.crearMensajeInfo("facturab generado");
			listaFactura=new ArrayList<DTOFactura>();
			total=0;
			iva=0;
			totalApagar=0;
		} catch (Exception e) {
			JSFUtil.crearMensajeError(e.getMessage());
		}
		return "";
	}

	public List<ProDetalle> getListaproCategorias() {
		return listaproCategorias;
	}

	public void setListaproCategorias(List<ProDetalle> listaproCategorias) {
		this.listaproCategorias = listaproCategorias;
	}

	public String getProCodigoCategoriaSeleccionado() {
		return proCodigoCategoriaSeleccionado;
	}

	public void setProCodigoCategoriaSeleccionado(String proCodigoCategoriaSeleccionado) {
		this.proCodigoCategoriaSeleccionado = proCodigoCategoriaSeleccionado;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public boolean isPanelColapsado() {
		return panelColapsado;
	}

	public void setPanelColapsado(boolean panelColapsado) {
		this.panelColapsado = panelColapsado;
	}

	public String getProCalidad() {
		return proCalidad;
	}

	public void setProCalidad(String proCalidad) {
		this.proCalidad = proCalidad;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public String getCedulaSeleccionado() {
		return cedulaSeleccionado;
	}

	public void setCedulaSeleccionado(String cedulaSeleccionado) {
		this.cedulaSeleccionado = cedulaSeleccionado;
	}

	public List<DTOFactura> getListaFactura() {
		return listaFactura;
	}

	public void setListaFactura(List<DTOFactura> listaFactura) {
		this.listaFactura = listaFactura;
	}

	public double getIva() {
		return iva;
	}

	public void setIva(double iva) {
		this.iva = iva;
	}

	public double getTotalApagar() {
		return totalApagar;
	}

	public void setTotalApagar(double totalApagar) {
		this.totalApagar = totalApagar;
	}

	public List<Cliente> getListadoClientes() {
		return listadoClientes;
	}

	public void setListadoClientes(List<Cliente> listadoClientes) {
		this.listadoClientes = listadoClientes;
	}

}
