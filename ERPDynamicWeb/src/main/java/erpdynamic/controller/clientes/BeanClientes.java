package erpdynamic.controller.clientes;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import erpdynamic.model.clientes.managers.ManagerClientes;
import erpdynamic.model.entities.Cliente;
import erpdynamic.util.JSF.JSFUtil;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

@Named
@SessionScoped
public class BeanClientes implements Serializable {
	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerClientes managerClientes;
	private List<Cliente> listaClientes;
	private Cliente clienteSeleccionado;
	private Cliente cliente;

	private boolean panelColapsado;

	public BeanClientes() {

	}

	@PostConstruct
	public void inicializar() {
		listaClientes = managerClientes.findAllCliente();
		cliente = new Cliente();
		panelColapsado = true;
	}

	public void actionListenerColapsarPanel() {
		panelColapsado = !panelColapsado;
	}

	public void actionListenerInsertarCliente() {
		try {
			
			managerClientes.insertarCliente(cliente);
			listaClientes=managerClientes.findAllCliente();
			cliente=new Cliente();
			JSFUtil.crearMensajeInfo("Datos de estudiante insertados.");
		} catch (Exception e) {
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerEliminarCliente(String cedula) {
		managerClientes.eliminarCliente(cedula);
		listaClientes = managerClientes.findAllCliente();
		JSFUtil.crearMensajeInfo("Cliente eliminado");
	}

	public void actionListenerSeleccionarCliente(Cliente cliente) {
		clienteSeleccionado = cliente;
	}

	public void actionListenerActualizarCliente() {
		try {
			managerClientes.actualizarCliente(clienteSeleccionado);
			listaClientes = managerClientes.findAllCliente();
			JSFUtil.crearMensajeInfo("Datos actualizados.");
		} catch (Exception e) {
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public List<Cliente> getListaClientes() {
		return listaClientes;
	}

	public void setListaClientes(List<Cliente> listaClientes) {
		this.listaClientes = listaClientes;
	}

	public Cliente getClienteSeleccionado() {
		return clienteSeleccionado;
	}

	public void setClienteSeleccionado(Cliente clienteSeleccionado) {
		this.clienteSeleccionado = clienteSeleccionado;
	}

	public boolean isPanelColapsado() {
		return panelColapsado;
	}

	public void setPanelColapsado(boolean panelColapsado) {
		this.panelColapsado = panelColapsado;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public String actionReporte(){                
		Map<String,Object> parametros=new HashMap<String,Object>();
		/*parametros.put("p_titulo_principal",p_titulo_principal);                
		 * parametros.put("p_titulo",p_titulo);*/                
		FacesContext context=FacesContext.getCurrentInstance();                
		ServletContext servletContext=(ServletContext)context.getExternalContext().getContext();                
		String ruta=servletContext.getRealPath("clientes/reporteClientes.jasper");                
		System.out.println(ruta);                
		HttpServletResponse response=(HttpServletResponse)context.getExternalContext().getResponse();                
		response.addHeader("Content-disposition","attachment;filename=reporte.pdf");                
		response.setContentType("application/pdf");
		try{                        
			Class.forName("org.postgresql.Driver");                        
			Connection connection =null;                        
			connection = DriverManager.getConnection("jdbc:postgresql://192.100.198.141:5432/spgordonm","spgordonm","1003676861");                        
			JasperPrint impresion=JasperFillManager.fillReport(ruta, parametros,connection);                        
			JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());                        
			context.getApplication().getStateManager().saveView( context );                        
			System.out.println("reporte generado.");                        
			context.responseComplete();
			}catch(Exception e){                        
				JSFUtil.crearMensajeError(e.getMessage());                       
				e.printStackTrace();
				}
		return"";
		}
}
