
package erpdynamic.model.thumano.dtos;

public class DTOCategoria {
	private int proCodigoCategoria;
	private int  cantidad;
	private String proMaterial; 
	private String proTipo;
	public DTOCategoria(int proCodigoCategoria, int cantidad, String proMaterial, String proTipo) {
		super();
		this.proCodigoCategoria = proCodigoCategoria;  
		this.cantidad = cantidad; 
		this.proMaterial = proMaterial;
		this.proTipo = proTipo;
	}
	public int getProCodigoCategoria() {
		return proCodigoCategoria;
	}
	public void setProCodigoCategoria(int proCodigoCategoria) {
		this.proCodigoCategoria = proCodigoCategoria;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public String getProMaterial() {
		return proMaterial;
	}
	public void setProMaterial(String proMaterial) {
		this.proMaterial = proMaterial;
	}
	public String getProTipo() {
		return proTipo;
	}
	public void setProTipo(String proTipo) {
		this.proTipo = proTipo;
	}
}

