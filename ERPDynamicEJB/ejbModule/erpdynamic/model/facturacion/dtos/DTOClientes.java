package erpdynamic.model.facturacion.dtos;

import java.util.ArrayList;
import java.util.List;



public class DTOClientes {
	private String id;
	private String apellidos;
	private String nombres;
	private String direccion;
	private String telefono;
	private String correo;
	List<DTOFactura> facturas;
	
	public DTOClientes(String id, String nombres, String apellidos,String direccion,String telefono, String correo) {
		this.id = id;
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.direccion=direccion;
		this.telefono=telefono;
		this.correo = correo;
		this.facturas = new ArrayList<DTOFactura>();
	}
	
	public List<DTOFactura> getFacturas() {
		return facturas;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getDereccion() {
		return direccion;
	}
	public void setDereccion(String dereccion) {
		this.direccion = dereccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
}
