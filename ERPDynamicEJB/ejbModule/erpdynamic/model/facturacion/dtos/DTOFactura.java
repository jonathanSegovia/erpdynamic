package erpdynamic.model.facturacion.dtos;



import erpdynamic.model.entities.ThmFacturaDetalle;

public class DTOFactura {
	private String descripcion;
	private String nombre;
	private int calidad;
	private int proDetalle;
	private double total;
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public int getCalidad() {
		return calidad;
	}
	public void setCalidad(int calidad) {
		this.calidad = calidad;
	}
	public int getProDetalle() {
		return proDetalle;
	}
	public void setProDetalle(int proDetalle) {
		this.proDetalle = proDetalle;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	
}
