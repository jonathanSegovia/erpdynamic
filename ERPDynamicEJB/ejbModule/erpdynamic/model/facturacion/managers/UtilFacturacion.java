package erpdynamic.model.facturacion.managers;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import erpdynamic.model.entities.ThmFacturaCabecera;
import erpdynamic.model.entities.ThmFacturaDetalle;


public class UtilFacturacion {
	
	public static double MINIMO_PARA_FACTURAR = 10;
	public static double IVA = 0.12;
	public static double DESCUENTO = 0.10;

	public static ArrayList<ThmFacturaDetalle> getNewListDetalleFactura() {
		return new ArrayList<ThmFacturaDetalle>();
	}
	
	public static ThmFacturaCabecera getNewFactura() {
		return new ThmFacturaCabecera();
	}
	
	public static BigDecimal getNewBigDecimal(String value, int scale) {
		return new BigDecimal(value.toString()).setScale(scale, RoundingMode.HALF_UP);
	}

	public static BigDecimal calculateGlobalSubtotal(List<ThmFacturaDetalle> items) {
		Double total = .0;
		for (ThmFacturaDetalle item : items) {
			total = total + item.getValor().doubleValue();
		}
		return getNewBigDecimal(total.toString(), 2);
	}

	public static BigDecimal calculateDifference(BigDecimal value, BigDecimal putOff) {
		Double difference = value.doubleValue() - putOff.doubleValue();
		return getNewBigDecimal(difference.toString(), 2);
	}

	public static BigDecimal calculateAdd(BigDecimal value, BigDecimal increase) {
		Double difference = value.doubleValue() + increase.doubleValue();
		return getNewBigDecimal(difference.toString(), 2);
	}

	public static BigDecimal calculateIVA(BigDecimal subtotal) {
		Double ivaValue = subtotal.doubleValue() * IVA;
		return getNewBigDecimal(ivaValue.toString(), 2);
	}

	public static BigDecimal calculateDescuento(BigDecimal subtotal) {
		Double descuentoValue = subtotal.doubleValue() * DESCUENTO;
		return getNewBigDecimal(descuentoValue.toString(), 2);
	}

	public static BigDecimal calculatePorcentageValue(BigDecimal value, Double pp) {
		Double porcentageValue = value.doubleValue() * pp;
		return getNewBigDecimal(porcentageValue.toString(), 2);
	}

}
