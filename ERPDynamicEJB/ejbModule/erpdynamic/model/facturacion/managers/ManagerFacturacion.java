package erpdynamic.model.facturacion.managers;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import erpdynamic.model.dtos.DTODetalleFactura;
import erpdynamic.model.entities.Cliente;
import erpdynamic.model.entities.ProDetalle;
import erpdynamic.model.entities.ThmFacturaCabecera;
import erpdynamic.model.entities.ThmFacturaDetalle;
import erpdynamic.model.facturacion.dtos.DTOFactura;

/**
 * Session Bean implementation class ManagerFacturacion
 */
@Stateless
@LocalBean
public class ManagerFacturacion {
	@PersistenceContext
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public ManagerFacturacion() {
        
    }
    
    public List<ProDetalle> finAllProDetalle(){
    	TypedQuery<ProDetalle> q = em.createQuery("SELECT p FROM ProDetalle p", ProDetalle.class);
    	return q.getResultList();
    }
    
    public List<Cliente> finAllCliente(){
    	TypedQuery<Cliente> q = em.createQuery("SELECT c FROM Cliente c", Cliente.class);
    	return q.getResultList();
    }
    
    public Cliente findClienteByCedula(String cedula) {
    	return em.find(Cliente.class, cedula);
    }
    
    public void insertarCliente(Cliente cliente) throws Exception {
    	if(findClienteByCedula(cliente.getCliCedula())!=null)
    		throw new Exception("Ya existe la cédula indicada.");
    	em.merge(cliente);
    	em.persist(cliente);
    }
    
    public DTOFactura crearfactura(int codigodetalle,int calidad){
    	ProDetalle pro=em.find(ProDetalle.class, codigodetalle);
    	DTOFactura fac=new DTOFactura();
    	fac.setProDetalle(codigodetalle);
    	fac.setDescripcion(pro.getProDescripcion());
    	fac.setCalidad(calidad);
    	fac.setTotal(pro.getProValor().doubleValue());
    	return fac;
    }
    
    public void insertarFactura(List<DTOFactura> listaFactura,String cedulaCliente) {
    	Cliente cliente=em.find(Cliente.class, cedulaCliente);
    	ThmFacturaCabecera acab=new ThmFacturaCabecera();
    	acab.setCliente(cliente);
    	acab.setFechaThmFacturaCabecera(new Date());
    	acab.setSubtotal(new BigDecimal(subtotalFactura(listaFactura)));
    	acab.setIva(new BigDecimal(0.12*(subtotalFactura(listaFactura))));
    	acab.setTotal(new BigDecimal(subtotalFactura(listaFactura)+(0.12*(subtotalFactura(listaFactura)))));
    	
    	List<ThmFacturaDetalle> listaDetalle=new ArrayList<ThmFacturaDetalle>();
    	acab.setThmFacturaDetalles(listaDetalle);
    	
    	for(DTOFactura det:listaFactura) {
    		ThmFacturaDetalle facDet=new ThmFacturaDetalle();
    		ProDetalle pro=em.find(ProDetalle.class, facDet.getProDetalle());
    		facDet.setThmFacturaCabecera(acab);
    		facDet.setProDetalle(pro);
    		facDet.setCalidad(det.getCalidad());
    		facDet.setValor(new BigDecimal(det.getTotal()));
    		listaDetalle.add(facDet);
    	}
    	
    	em.persist(acab);
    }
    
    public double totalFactura(List<DTODetalleFactura> listaDetalleFactura) {
    	double total=0;
    	for(DTODetalleFactura d:listaDetalleFactura)
    		total+=d.getSubtotal();
    	return total;
    }
    public double subtotalFactura(List<DTOFactura> listaFactura) {
    	double total=0;
    	for(DTOFactura d:listaFactura)
    		total+=d.getTotal();
    	return total;
    }
    
    public double iva(List<DTODetalleFactura> listaDetalleFactura) {
    	double total=0;
    	double iva=0;
    	for(DTODetalleFactura d:listaDetalleFactura)
    		total+=d.getSubtotal();
    		iva=total*0.12;
    	return iva;
    }
}
