package erpdynamic.model.clientes.managers;

import java.math.BigDecimal;
import java.util.List;
import java.util.Random;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import erpdynamic.model.core.managers.ManagerDAO;
import erpdynamic.model.entities.Cliente;
import erpdynamic.model.entities.ThmEmpleado;

/**
 * Session Bean implementation class ManagerClientes
 */
@Stateless
@LocalBean
public class ManagerClientes {
	@EJB
	private ManagerDAO mDAO;
	@PersistenceContext
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public ManagerClientes() {

	}

	public List<Cliente> findAllCliente() {
		TypedQuery<Cliente> q = em.createQuery("select c from Cliente c order by c.cliNombres", Cliente.class);
		return q.getResultList();
	}

	public Cliente findClienteByCedula(String cedula) {
		return em.find(Cliente.class, cedula);
	}

	public void insertarCliente(Cliente cliente) throws Exception {
		Cliente cli=new Cliente();
		cli.setCliCedula(cliente.getCliCedula());
		cli.setCliNombres(cliente.getCliNombres());
		cli.setCliApellidos(cliente.getCliApellidos());
		cli.setCliDireccion(cliente.getCliDireccion());
		cli.setCliTelefono(cliente.getCliTelefono());
		cli.setCliCorreo(cliente.getCliCorreo());
		em.persist(cli);
	}

	public void eliminarCliente(String cedula) {
		Cliente cliente = findClienteByCedula(cedula);
		if (cliente != null)
			em.remove(cliente);
	}

	public void actualizarCliente(Cliente cliente) throws Exception {
		Cliente e = findClienteByCedula(cliente.getCliCedula());
		if (e == null)
			throw new Exception("No existe el cliente con la cédula especificada");
		e.setCliApellidos(cliente.getCliApellidos());
		e.setCliNombres(cliente.getCliNombres());
		e.setCliDireccion(cliente.getCliDireccion());
		e.setCliTelefono(cliente.getCliTelefono());
		e.setCliCorreo(cliente.getCliCorreo());
		em.merge(e);
	}

}
