package erpdynamic.model.productos.managers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import erpdynamic.model.core.managers.ManagerDAO;
import erpdynamic.model.entities.Cliente;
import erpdynamic.model.entities.ProCategoria;
import erpdynamic.model.entities.ProDetalle;
import erpdynamic.model.entities.SegUsuario;
import erpdynamic.model.entities.ThmCargo;
import erpdynamic.model.thumano.dtos.DTOCategoria;
//import erpdynamic.model.thumano.dtos.DTOCategoria;
import erpdynamic.model.thumano.dtos.DTOThmCargo;

/**
 * Session Bean implementation class ManagerProductos
 */
@Stateless
@LocalBean
public class ManagerProductos {
	@EJB
	private ManagerDAO mDAO;
	@PersistenceContext
	private EntityManager em;

    /**
     * Default constructor. 
     */
    public ManagerProductos() {
        // TODO Auto-generated constructor stub
    }

    public List<ProDetalle> findAllProDetalle(){
    	return mDAO.findAll(ProDetalle.class, "proDescripcion");
    }
    public List<ProDetalle> findAllProDetalles(){
    	TypedQuery<ProDetalle> q=em.createQuery("select p from ProDetalle p order by p.proDescripcion", ProDetalle.class);
    	return q.getResultList();
    }
    
    public ProDetalle findAllProductoByCod(int proCodigoDetalle) {
    	return em.find(ProDetalle.class, proCodigoDetalle);
    }
    public void insertarProducto(ProDetalle pro) throws Exception{
    	if(findAllProductoByCod(pro.getProCodigoDetalle())!=null)
    		throw new Exception("Ya existe el producto indicada.");
    	//em.persist(pro);
    	mDAO.insertar(pro);
    }
    public void insertarProd(ProDetalle nuevoproducto) throws Exception {
    	nuevoproducto.setProCodigoDetalle(null);
    	mDAO.insertar(nuevoproducto);
    }
    public ProDetalle insertarprodcutoes(ProDetalle nuevoproducto,int pro_codigo_detalle, ProCategoria categoria, String calidad, String descripcion, double valor) throws Exception {
    	ProDetalle nuevo= new ProDetalle();
    	nuevo.setProCodigoDetalle(10);
    	nuevo.setProCategoria(categoria);
    	nuevo.setProCalidad(calidad);//prestamo entre 0 y 100
    	nuevo.setProDescripcion(descripcion);
    	nuevo.setProValor(new BigDecimal(valor));
    	em.persist(nuevo);
    	//mDAO.insertar(nuevo);
    	return nuevo;
    }
    public void insertarProductos(int prod, int ced, String proCalidad, String proDescripcion,BigDecimal proValor, ProCategoria proCategoria) throws Exception {
    	ProDetalle  e = new ProDetalle();
    	e.setProCodigoDetalle(prod);
    	e.setProCalidad(proCalidad);
    	e.setProDescripcion(proDescripcion);
    	e.setProValor(proValor);
    	e.setProCategoria(proCategoria);
    	//em.persist(e);
    	mDAO.insertar(e);
    	
    }
    
    public void eliminarProducto(int proproCodigoDetalle) {
    	ProDetalle prod=findAllProductoByCod(proproCodigoDetalle);
    	if(prod!=null)
    		em.remove(prod);
    
    }
    
    public void actualizarProducto(ProDetalle prod, int proCodigoCategoria) throws Exception {

    	ProDetalle p=(ProDetalle) mDAO.findById(ProDetalle.class, prod.getProCodigoDetalle());
    	//ProDetalle p=findAllProductoByCod(prod.getProCodigoDetalle());
    	if(p==null)
    		throw new Exception("No existe el producto especificado");
    	p.setProCalidad(prod.getProCalidad());
    	p.setProDescripcion(prod.getProDescripcion());
    	p.setProValor(prod.getProValor());
    	p.setProCategoria(findByproCodigoCategoria(proCodigoCategoria));
    	em.merge(p);
    }

	//Categoria:
	
    public ProCategoria findByproCodigoCategoria(int proCodigoCategoria) throws Exception {
    	return(ProCategoria) mDAO.findById(ProCategoria.class, proCodigoCategoria);
    }
    
    public List<ProCategoria> findAllProCategorias(){
    	return mDAO.findAll(ProCategoria.class, "proTipo");
    }
    
    public List<DTOCategoria> findAllDtoCategorias(){
        List<DTOCategoria> listaDTO=new ArrayList<DTOCategoria>();
        for(ProCategoria cat:findAllProCategorias()) {
            
            DTOCategoria nuevoDTO= new DTOCategoria(cat.getProCodigoCategoria(), cat.getCantidad(), cat.getProMaterial(), cat.getProTipo());
            
            listaDTO.add(nuevoDTO);
        }
        return listaDTO;
    }
    
    
    public void insertarCategoria() throws Exception {
    	ProCategoria categoria=new ProCategoria();
    	categoria.setProCodigoCategoria(Integer.parseInt("10"));
    	categoria.setCantidad(10);
    	categoria.setProMaterial("Vinil");
    	categoria.setProTipo("Impresion");
    	mDAO.insertar(categoria);
    }
    public void actualizarCategoria(ProCategoria prodC) throws Exception {
    	ProCategoria pc=(ProCategoria) mDAO.findById(ProCategoria.class, prodC.getProCodigoCategoria());

    	//ProDetalle p=(ProDetalle) mDAO.findById(ProDetalle.class, prod.getProCodigoDetalle());
    	//ProDetalle p=findAllProductoByCod(prod.getProCodigoDetalle());
    	if(pc!=null)
    		throw new Exception("No existe la categoria especificado");
    	pc.setProTipo(prodC.getProTipo());
    	pc.setProMaterial(prodC.getProMaterial());;
    	pc.setCantidad(prodC.getCantidad());
    	em.merge(pc);
    }
}
