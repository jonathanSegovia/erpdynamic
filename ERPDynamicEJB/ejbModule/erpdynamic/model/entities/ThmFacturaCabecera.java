package erpdynamic.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the thm_factura_cabecera database table.
 * 
 */
@Entity
@Table(name="thm_factura_cabecera")
@NamedQuery(name="ThmFacturaCabecera.findAll", query="SELECT t FROM ThmFacturaCabecera t")
public class ThmFacturaCabecera implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_thm_factura_cabecera", unique=true, nullable=false)
	private Integer idThmFacturaCabecera;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_thm_factura_cabecera", nullable=false)
	private Date fechaThmFacturaCabecera;

	@Column(nullable=false, precision=7, scale=2)
	private BigDecimal iva;

	@Column(nullable=false, precision=7, scale=2)
	private BigDecimal subtotal;

	@Column(nullable=false, precision=7, scale=2)
	private BigDecimal total;

	//bi-directional many-to-one association to Cliente
	@ManyToOne
	@JoinColumn(name="cli_cedula", nullable=false)
	private Cliente cliente;

	//bi-directional many-to-one association to ThmFacturaDetalle
	@OneToMany(mappedBy="thmFacturaCabecera")
	private List<ThmFacturaDetalle> thmFacturaDetalles;

	public ThmFacturaCabecera() {
	}

	public Integer getIdThmFacturaCabecera() {
		return this.idThmFacturaCabecera;
	}

	public void setIdThmFacturaCabecera(Integer idThmFacturaCabecera) {
		this.idThmFacturaCabecera = idThmFacturaCabecera;
	}

	public Date getFechaThmFacturaCabecera() {
		return this.fechaThmFacturaCabecera;
	}

	public void setFechaThmFacturaCabecera(Date fechaThmFacturaCabecera) {
		this.fechaThmFacturaCabecera = fechaThmFacturaCabecera;
	}

	public BigDecimal getIva() {
		return this.iva;
	}

	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	public BigDecimal getSubtotal() {
		return this.subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	public BigDecimal getTotal() {
		return this.total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public Cliente getCliente() {
		return this.cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<ThmFacturaDetalle> getThmFacturaDetalles() {
		return this.thmFacturaDetalles;
	}

	public void setThmFacturaDetalles(List<ThmFacturaDetalle> thmFacturaDetalles) {
		this.thmFacturaDetalles = thmFacturaDetalles;
	}

	public ThmFacturaDetalle addThmFacturaDetalle(ThmFacturaDetalle thmFacturaDetalle) {
		getThmFacturaDetalles().add(thmFacturaDetalle);
		thmFacturaDetalle.setThmFacturaCabecera(this);

		return thmFacturaDetalle;
	}

	public ThmFacturaDetalle removeThmFacturaDetalle(ThmFacturaDetalle thmFacturaDetalle) {
		getThmFacturaDetalles().remove(thmFacturaDetalle);
		thmFacturaDetalle.setThmFacturaCabecera(null);

		return thmFacturaDetalle;
	}

}