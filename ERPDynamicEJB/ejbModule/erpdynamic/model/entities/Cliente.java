package erpdynamic.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the cliente database table.
 * 
 */
@Entity
@Table(name="cliente")
@NamedQuery(name="Cliente.findAll", query="SELECT c FROM Cliente c")
public class Cliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="cli_cedula", unique=true, nullable=false, length=10)
	private String cliCedula;

	@Column(name="cli_apellidos", nullable=false, length=100)
	private String cliApellidos;

	@Column(name="cli_correo", nullable=false, length=100)
	private String cliCorreo;

	@Column(name="cli_direccion", nullable=false, length=100)
	private String cliDireccion;

	@Column(name="cli_nombres", nullable=false, length=100)
	private String cliNombres;

	@Column(name="cli_telefono", nullable=false, length=10)
	private String cliTelefono;

	//bi-directional many-to-one association to ThmFacturaCabecera
	@OneToMany(mappedBy="cliente")
	private List<ThmFacturaCabecera> thmFacturaCabeceras;

	public Cliente() {
	}

	public String getCliCedula() {
		return this.cliCedula;
	}

	public void setCliCedula(String cliCedula) {
		this.cliCedula = cliCedula;
	}

	public String getCliApellidos() {
		return this.cliApellidos;
	}

	public void setCliApellidos(String cliApellidos) {
		this.cliApellidos = cliApellidos;
	}

	public String getCliCorreo() {
		return this.cliCorreo;
	}

	public void setCliCorreo(String cliCorreo) {
		this.cliCorreo = cliCorreo;
	}

	public String getCliDireccion() {
		return this.cliDireccion;
	}

	public void setCliDireccion(String cliDireccion) {
		this.cliDireccion = cliDireccion;
	}

	public String getCliNombres() {
		return this.cliNombres;
	}

	public void setCliNombres(String cliNombres) {
		this.cliNombres = cliNombres;
	}

	public String getCliTelefono() {
		return this.cliTelefono;
	}

	public void setCliTelefono(String cliTelefono) {
		this.cliTelefono = cliTelefono;
	}

	public List<ThmFacturaCabecera> getThmFacturaCabeceras() {
		return this.thmFacturaCabeceras;
	}

	public void setThmFacturaCabeceras(List<ThmFacturaCabecera> thmFacturaCabeceras) {
		this.thmFacturaCabeceras = thmFacturaCabeceras;
	}

	public ThmFacturaCabecera addThmFacturaCabecera(ThmFacturaCabecera thmFacturaCabecera) {
		getThmFacturaCabeceras().add(thmFacturaCabecera);
		thmFacturaCabecera.setCliente(this);

		return thmFacturaCabecera;
	}

	public ThmFacturaCabecera removeThmFacturaCabecera(ThmFacturaCabecera thmFacturaCabecera) {
		getThmFacturaCabeceras().remove(thmFacturaCabecera);
		thmFacturaCabecera.setCliente(null);

		return thmFacturaCabecera;
	}

}