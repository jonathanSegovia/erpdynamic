package erpdynamic.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the pro_categoria database table.
 * 
 */
@Entity
@Table(name="pro_categoria")
@NamedQuery(name="ProCategoria.findAll", query="SELECT p FROM ProCategoria p")
public class ProCategoria implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pro_codigo_categoria", unique=true, nullable=false)
	private Integer proCodigoCategoria;

	@Column(nullable=false)
	private Integer cantidad;

	@Column(name="pro_material", nullable=false, length=50)
	private String proMaterial;

	@Column(name="pro_tipo", nullable=false, length=10)
	private String proTipo;

	//bi-directional many-to-one association to ProDetalle
	@OneToMany(mappedBy="proCategoria")
	private List<ProDetalle> proDetalles;

	public ProCategoria() {
	}

	public Integer getProCodigoCategoria() {
		return this.proCodigoCategoria;
	}

	public void setProCodigoCategoria(Integer proCodigoCategoria) {
		this.proCodigoCategoria = proCodigoCategoria;
	}

	public Integer getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public String getProMaterial() {
		return this.proMaterial;
	}

	public void setProMaterial(String proMaterial) {
		this.proMaterial = proMaterial;
	}

	public String getProTipo() {
		return this.proTipo;
	}

	public void setProTipo(String proTipo) {
		this.proTipo = proTipo;
	}

	public List<ProDetalle> getProDetalles() {
		return this.proDetalles;
	}

	public void setProDetalles(List<ProDetalle> proDetalles) {
		this.proDetalles = proDetalles;
	}

	public ProDetalle addProDetalle(ProDetalle proDetalle) {
		getProDetalles().add(proDetalle);
		proDetalle.setProCategoria(this);

		return proDetalle;
	}

	public ProDetalle removeProDetalle(ProDetalle proDetalle) {
		getProDetalles().remove(proDetalle);
		proDetalle.setProCategoria(null);

		return proDetalle;
	}

}