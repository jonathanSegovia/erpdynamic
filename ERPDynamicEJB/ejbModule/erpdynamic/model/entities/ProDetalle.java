package erpdynamic.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the pro_detalle database table.
 * 
 */
@Entity
@Table(name="pro_detalle")
@NamedQuery(name="ProDetalle.findAll", query="SELECT p FROM ProDetalle p")
public class ProDetalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pro_codigo_detalle", unique=true, nullable=false)
	private Integer proCodigoDetalle;

	@Column(name="pro_calidad", nullable=false, length=10)
	private String proCalidad;

	@Column(name="pro_descripcion", nullable=false, length=100)
	private String proDescripcion;

	@Column(name="pro_valor", nullable=false, precision=7, scale=2)
	private BigDecimal proValor;

	//bi-directional many-to-one association to ProCategoria
	@ManyToOne
	@JoinColumn(name="pro_codigo_categoria", nullable=false)
	private ProCategoria proCategoria;

	//bi-directional many-to-one association to ThmFacturaDetalle
	@OneToMany(mappedBy="proDetalle")
	private List<ThmFacturaDetalle> thmFacturaDetalles;

	public ProDetalle() {
	}

	public Integer getProCodigoDetalle() {
		return this.proCodigoDetalle;
	}

	public void setProCodigoDetalle(Integer proCodigoDetalle) {
		this.proCodigoDetalle = proCodigoDetalle;
	}

	public String getProCalidad() {
		return this.proCalidad;
	}

	public void setProCalidad(String proCalidad) {
		this.proCalidad = proCalidad;
	}

	public String getProDescripcion() {
		return this.proDescripcion;
	}

	public void setProDescripcion(String proDescripcion) {
		this.proDescripcion = proDescripcion;
	}

	public BigDecimal getProValor() {
		return this.proValor;
	}

	public void setProValor(BigDecimal proValor) {
		this.proValor = proValor;
	}

	public ProCategoria getProCategoria() {
		return this.proCategoria;
	}

	public void setProCategoria(ProCategoria proCategoria) {
		this.proCategoria = proCategoria;
	}

	public List<ThmFacturaDetalle> getThmFacturaDetalles() {
		return this.thmFacturaDetalles;
	}

	public void setThmFacturaDetalles(List<ThmFacturaDetalle> thmFacturaDetalles) {
		this.thmFacturaDetalles = thmFacturaDetalles;
	}

	public ThmFacturaDetalle addThmFacturaDetalle(ThmFacturaDetalle thmFacturaDetalle) {
		getThmFacturaDetalles().add(thmFacturaDetalle);
		thmFacturaDetalle.setProDetalle(this);

		return thmFacturaDetalle;
	}

	public ThmFacturaDetalle removeThmFacturaDetalle(ThmFacturaDetalle thmFacturaDetalle) {
		getThmFacturaDetalles().remove(thmFacturaDetalle);
		thmFacturaDetalle.setProDetalle(null);

		return thmFacturaDetalle;
	}

}