package erpdynamic.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the thm_factura_detalle database table.
 * 
 */
@Entity
@Table(name="thm_factura_detalle")
@NamedQuery(name="ThmFacturaDetalle.findAll", query="SELECT t FROM ThmFacturaDetalle t")
public class ThmFacturaDetalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_thm_factura_detalle", unique=true, nullable=false)
	private Integer idThmFacturaDetalle;

	@Column(nullable=false)
	private Integer calidad;

	@Column(nullable=false, length=100)
	private String descripcion;

	@Column(nullable=false, precision=7, scale=2)
	private BigDecimal valor;

	//bi-directional many-to-one association to ProDetalle
	@ManyToOne
	@JoinColumn(name="pro_codigo_detalle", nullable=false)
	private ProDetalle proDetalle;

	//bi-directional many-to-one association to ThmFacturaCabecera
	@ManyToOne
	@JoinColumn(name="id_thm_factura_cabecera", nullable=false)
	private ThmFacturaCabecera thmFacturaCabecera;

	public ThmFacturaDetalle() {
	}

	public Integer getIdThmFacturaDetalle() {
		return this.idThmFacturaDetalle;
	}

	public void setIdThmFacturaDetalle(Integer idThmFacturaDetalle) {
		this.idThmFacturaDetalle = idThmFacturaDetalle;
	}

	public Integer getCalidad() {
		return this.calidad;
	}

	public void setCalidad(Integer calidad) {
		this.calidad = calidad;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public BigDecimal getValor() {
		return this.valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public ProDetalle getProDetalle() {
		return this.proDetalle;
	}

	public void setProDetalle(ProDetalle proDetalle) {
		this.proDetalle = proDetalle;
	}

	public ThmFacturaCabecera getThmFacturaCabecera() {
		return this.thmFacturaCabecera;
	}

	public void setThmFacturaCabecera(ThmFacturaCabecera thmFacturaCabecera) {
		this.thmFacturaCabecera = thmFacturaCabecera;
	}

}